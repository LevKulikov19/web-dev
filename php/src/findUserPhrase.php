<?php
include '../private/db.php';
$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);

try {
    $conn->set_charset("utf8");

    if ($conn->connect_error)
        throw new Exception("Нет подключения к БД");

    if (isset($_POST['search'])) {
        $search_query = "SELECT * FROM User";
        $where_clause = '';
        $user_search = $_POST['search'];
        $search_words = explode(' ', $user_search);

        foreach ($search_words as $word) {
            if (!empty($word)) {
                $where_clause .= "firstName LIKE '%$word%' OR ";
            }
        }

        $where_clause = rtrim($where_clause, " OR ");
        if (!empty($where_clause)) {
            $search_query .= " WHERE $where_clause";
        }

        $result = $conn->query($search_query);
        $response = array();
        if ($result && $result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $response[] = $row;
            }
            header('Content-Type: application/json');
            echo json_encode($response);
        } else
            throw new Exception("По запросу ничего не найдено");
    } else
        throw new Exception("Введите запрос для поиска");
}
catch (Exception $e)
{
    http_response_code(404);
    echo $e->getMessage();
}
finally {
    $conn->close();
}
