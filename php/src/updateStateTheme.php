<?php
include '../private/db.php';

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);
$conn->set_charset("utf8");

$token = $_COOKIE["token"];
try {
    var_dump($_POST["darkTheme"]);
    $sql = "UPDATE User SET darkTheme=? WHERE token=?";
    $stmt = $conn->prepare($sql);
    $newTheme = $_POST["darkTheme"] == 'true' ? 1 : 0;
    $stmt->bind_param("is", $newTheme, $token);

    if ($stmt->execute()) {
        echo "Тема успешно изменена.";
    } else {
        echo "Ошибка: " . $stmt->error;
    }
} finally {
    $stmt->close();
}