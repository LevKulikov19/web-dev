<!DOCTYPE html>
<html>
<head>
    <title>Форма регистрации</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/darkTheme.js" defer></script>
    <link rel="stylesheet" href="css/darkTheme.css">
    <script src='https://www.google.com/recaptcha/api.js' defer></script>
    <script src="js/cookieUtils.js" defer></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var form = document.querySelector('form.signup');
            form.addEventListener('submit', function(e) {
                e.preventDefault();
                var al = form.querySelector(".terms-label");
                var recaptcha = grecaptcha.getResponse();
                    if (!form.querySelector('#terms').checked) {
                        al.innerText = "Пожалуйста, примите условия использования сервиса перед отправкой формы.";
                        al.style.display = "block";
                    } else {
                        if (recaptcha.length == 0) {
                            al.style.display = "block";
                            al.innerText = "Пройдите recaptcha";
                        }
                        else {
                        al.style.display = "none";

                        var formData = new FormData(form);
                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", window.location.origin + "/signupAction", true);
                        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        xhr.send(new URLSearchParams(formData).toString());

                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == 4) {
                                var status = xhr.status;
                                var response = xhr.responseText;
                                if (status >= 200 && status < 300) {
                                    form.reset();
                                    console.log("Успешно: " + response);
                                    setCookie("token", response, 1);
                                    window.location.href = window.location.origin + "/admin";
                                } else {
                                    al.style.display = "block";
                                    al.innerText = response;
                                    console.log(response);
                                    grecaptcha.reset();
                                }
                            }
                        };
                    }
                }
            });
        });
    </script>
</head>
<body class="light-mode">
    <header class="d-flex flex-row-reverse w-75 p-3">
        <button id="themeButton" class="btn btn-secondary mt-3 ">Темная тема</button>
    </header>
    <div class="container">
        <h1 class="p-4 text-center">Форма регистрации</h1>
        <form class="signup">
            <div class="form-group">
                <label for="firstName">Имя</label>
                <input type="text" class="form-control"  name="firstName" id="firstName" pattern="[a-zA-Zа-яА-Я]{2,15}" required>
            </div>
            <div class="form-group">
                <label for="lastName">Фамилия</label>
                <input type="text" class="form-control"  name="lastName" id="lastName" pattern="^([a-zA-Zа-яА-Я-]{2,15}\s?){1,2}$" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control"  name="email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
            </div>
            <div class="form-group">
                <label for="login">Логин</label>
                <input type="text" class="form-control"  name="login" id="login" pattern=".{6,}" required>
            </div>
            <div class="form-group">
                <label for="password">Пароль</label>
                <div class="input-group">
                    <input type="password" class="form-control" name="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                    <div class="input-group-append">
                        <button id="togglePassword" type="button" class="btn btn-light"><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="confirmPassword">Подтверждение пароля</label>
                <div class="input-group">
                    <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" required>
                    <div class="input-group-append">
                        <button id="toggleConfirmPassword" type="button" class="btn btn-light"><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="age">Возраст</label>
                <select class="form-control" name="age" id="age">
                    <option>Мне 18 лет</option>
                    <option>Нет 18 лет</option>
                </select>
            </div>
            <div class="form-group">
                <label>Пол</label><br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="male" value="male" checked>
                    <label class="form-check-label" for="male">Мужской</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="female" value="female">
                    <label class="form-check-label" for="female">Женский</label>
                </div>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="terms" id="terms">
                <label class="form-check-label" for="terms">Принимаю правила пользования сервисом</label>
            </div>
            <div class="g-recaptcha" data-sitekey="6LfoEJQpAAAAAE87zdU5FMEawdhZrHRwiN2c5Yxs"></div>
            <div class="alert alert-danger terms-label mt-2" role="alert" style="display: none">
                Ошибка.
            </div>
            <button type="submit" class="btn btn-primary mt-2">Отправить</button>
        </form>
        <a href="/"><button type="submit" class="btn btn-info mt-4">Вернуться к форме входа</button></a>
    </div>
    <script>
        var password = document.getElementById("password");
        var confirmPassword = document.getElementById("confirmPassword");
        var togglePassword = document.getElementById("togglePassword");
        var toggleConfirmPassword = document.getElementById("toggleConfirmPassword");

        togglePassword.addEventListener('click', function (e) {
            toggleVisibility(password, this);
        });

        toggleConfirmPassword.addEventListener('click', function (e) {
            toggleVisibility(confirmPassword, this);
        });

        function toggleVisibility(input, button) {
            const type = input.getAttribute('type') === 'password' ? 'text' : 'password';
            input.setAttribute('type', type);
            button.children[0].classList.toggle('fa-eye-slash');
        }

    </script>
</body>
</html>
