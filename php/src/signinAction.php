<?php
include '../private/db.php';
include '../private/errorLogin.php';

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);
$conn->set_charset("utf8");

try {
    if ($conn->connect_error)
        throw new Exception(code: 1000);

    $user = "";

    if ($_SERVER["REQUEST_METHOD"] != "POST") throw new Exception(code: 1000);

    if (count($_POST) != 3) throw new Exception(code: 1000);
    $recaptchaSecret = '6LfoEJQpAAAAAC9rqJlmGETzGfrS0P64X3bl50Kw';
    $recaptchaResponse = $_POST['g-recaptcha-response'];

    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$recaptchaSecret&response=$recaptchaResponse");
    $response = json_decode($response, true);

    if ($response["success"] !== true) throw new Exception(code: 113);

    $login = $_POST["login"];
    $password = $_POST["password"];
    if (empty($login))
        throw new Exception(code: 4);
    if (empty($password))
        throw new Exception(code: 5);

    $stmt = $conn->prepare("SELECT * FROM User WHERE login = '". $login ."'");
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_assoc();
    $stmt->close();
    if ($user == null) throw new Exception(code: 112);
    if (!password_verify($password, $user['password'])) throw new Exception(code: 112);

    echo $user['token'];
}
catch (Exception $e) {
    http_response_code(404);
    echo error_code_to_message($e->getCode());
}
finally {
    $conn->close();
}