<?php
include '../private/db.php';

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);
$conn->set_charset("utf8");

$token = $_COOKIE["token"];
try {
    $stmt = $conn->prepare("SELECT * FROM User WHERE token = '". $token ."'");
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_assoc();
    $stmt->close();

    if ($user != null)
    {
        $sql = "UPDATE User SET token=NULL WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $userId);

        if ($stmt->execute()) {
            echo "Выход произведен.";
        } else {
            echo "Ошибка: " . $stmt->error;
        }
    }

} finally {
    $stmt->close();
}