<?php
include '../private/errorLogin.php';
include '../private/db.php';

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);
$conn->set_charset("utf8");
try {
    if ($conn->connect_error)
        throw new Exception(code: 1000);

    $user = "";
    if ($_SERVER["REQUEST_METHOD"] != "POST") throw new Exception(code: 1000);
    if (count($_POST) != 10) throw new Exception(code: 1000);

    $recaptchaSecret = '6LfoEJQpAAAAAC9rqJlmGETzGfrS0P64X3bl50Kw';
    $recaptchaResponse = $_POST['g-recaptcha-response'];

    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$recaptchaSecret&response=$recaptchaResponse");
    $response = json_decode($response, true);

    if ($response["success"] !== true) throw new Exception(code: 113);

    $login = $_POST["login"];
    $password = $_POST["password"];
    if (empty($login))
        throw new Exception(code: 4);
    if (empty($password))
        throw new Exception(code: 5);

    $name = $_POST["firstName"];
    $surname = $_POST["lastName"];
    $email = $_POST["email"];
    $confirmPassword = $_POST["confirmPassword"];
    $age = $_POST["age"];
    $gender = $_POST["gender"];
    $terms = $_POST["terms"];

    if (empty($name))
        throw new Exception(code: 1);
    if (empty($surname))
        throw new Exception(code: 2);
    if (empty($email))
        throw new Exception(code: 3);
    if (empty($login))
        throw new Exception(code: 4);
    if (empty($password))
        throw new Exception(code: 5);
    if (empty($confirmPassword))
        throw new Exception(code: 6);
    if (empty($age))
        throw new Exception(code: 7);
    if (empty($gender))
        throw new Exception(code: 8);
    if (empty($terms))
        throw new Exception(code: 9);

    if (!validate_name($name))
        throw new Exception(code: 101);
    if (!validate_surname($surname))
        throw new Exception(code: 102);
    if (!validate_email($email))
        throw new Exception(code: 103);
    if (!validate_login($login))
        throw new Exception(code: 104);
    if (!validate_password($password))
        throw new Exception(code: 105);
    if (!validate_confirm_password($confirmPassword, $password))
        throw new Exception(code: 106);
    if (!validate_age($age))
        throw new Exception(code: 107);
    if (!validate_gender($gender))
        throw new Exception(code: 108);
    if (!validate_terms($terms))
        throw new Exception(code: 109);

    $stmt = $conn->prepare("SELECT * FROM User WHERE login = ?");
    $stmt->bind_param("s", $login);
    $stmt->execute();
    $result = $stmt->get_result();
    if (0 < $result->num_rows)
        throw new Exception(code: 110);

    $stmt = $conn->prepare("SELECT * FROM User WHERE email = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();
    if (0 < $result->num_rows)
        throw new Exception(code: 111);

    $stmt = $conn->prepare(
        "INSERT INTO User (firstName, lastName, email, login, password, age, gender, token) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

    $password_hashed = password_hash($password, PASSWORD_DEFAULT);
    $age_db = $age == "Мне 18 лет";
    $gender_db = $gender == "male";
    $token = uniqid();
    $stmt->bind_param("sssssiss", $name, $surname, $email, $login, $password_hashed, $age_db, $gender_db, $token);
    $stmt->execute();
    $stmt->close();

    echo $token;
}
catch (Exception $e)
{
    http_response_code(404);
    echo error_code_to_message($e->getCode());
} finally {
    $conn->close();
}

function test_input($data): string
{
    $data = trim($data);
    $data = stripslashes($data);
    return htmlspecialchars($data);
}

function validate_name($name): bool
{
    $name = test_input($name);
    $pattern = "/^[a-zA-Zа-яА-Я]{2,15}$/u";
    return preg_match($pattern, $name);
}

function validate_surname($name): bool
{
    $name = test_input($name);
    $pattern = "/^([a-zA-Zа-яА-Я-]{2,15}\s?){1,2}$/u";
    return preg_match_all($pattern, $name, $matches, PREG_SET_ORDER, 0);
}


function validate_email($email): bool
{
    $email = test_input($email);
    return boolval(filter_var($email, FILTER_VALIDATE_EMAIL));
}

function validate_login($login): bool
{
    $login = test_input($login);
    return preg_match("/^[a-zA-Z0-9]*$/", $login);
}

function validate_password($password): bool
{
    $password = test_input($password);
    return preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/", $password);
}

function validate_confirm_password($confirmPassword, $password): bool
{
    $confirmPassword = test_input($confirmPassword);
    return $confirmPassword == $password;
}

function validate_age($age): bool
{
    $age = test_input($age);
    return ($age == "Мне 18 лет") OR ($age == "Нет 18 лет");
}

function validate_gender($gender): bool
{
    $gender = test_input($gender);
    return ($gender = "male") OR ($gender = "female");
}

function validate_terms($terms): bool
{
    return $terms == "on";
}