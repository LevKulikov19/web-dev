<?php
include '../private/admin.php';
include '../private/errorLogin.php';
include '../private/db.php';

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);
$conn->set_charset("utf8");

try {
    $user = "";

    if ($conn->connect_error)
        throw new Exception(code: 1000);
    if (array_key_exists('token', $_COOKIE))
    {
        $token = $_COOKIE["token"];
        $stmt = $conn->prepare("SELECT * FROM User WHERE token = '". $token ."'");
        $stmt->execute();
        $result = $stmt->get_result();
        $user = $result->fetch_assoc();
        $stmt->close();

        if ($user == null) throw new Exception(code: 1);

        setcookie("darkTheme", $user['darkTheme']);
        render_admin(
            $user,
            getAllSurname($conn),
            getUserCount($conn),
            getUserCountLastMonth($conn),
            getUserLast($conn)
        );
    }
    else
    {
        render_admin_guest();
    }
}
catch (Exception $e)
{
    render_admin_guest();
} finally {
    $conn->close();
}

function getAllSurname($conn): array
{
    $stmt = $conn->prepare("SELECT lastName FROM User");
    $stmt->execute();
    $result = $stmt->get_result();
    $surnames = $result->fetch_all(MYSQLI_ASSOC);
    $surnames = array_column($surnames, 'lastName');
    return $surnames;
}

function getUserCount($conn): int
{
    $stmt = $conn->prepare("SELECT COUNT(*) FROM User");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_row()[0];
}

function getUserCountLastMonth($conn): int
{
    $date_array = getdate();
    $begin_date = date("Y-m-d", mktime(0, 0, 0, $date_array['mon'], 1, $date_array['year']));
    $end_date = date("Y-m-d", mktime(0, 0, 0, $date_array['mon'] + 1, 0, $date_array['year']));

    $query = "SELECT COUNT(id) FROM User WHERE dateCreate >= '$begin_date' AND dateCreate <= '$end_date'";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_row()[0];
}

function getUserLast($conn): array
{
    $query = "SELECT * FROM User ORDER BY dateCreate DESC LIMIT 0,1";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_assoc();
}
