<?php
include '../private/db.php';

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbnameDB);

try {
    $conn->set_charset("utf8");

    if ($conn->connect_error)
        throw new Exception("Нет подключения к БД");

    if (isset($_POST['search'])) {
        $user_search = $_POST['search'];

        if (!empty($user_search)) {
            $query_usersearch = "SELECT * FROM User WHERE firstName LIKE '%$user_search%'";
            $result_usersearch = $conn->query($query_usersearch);

            if ($result_usersearch->num_rows > 0) {
                $users = array();
                while ($row = $result_usersearch->fetch_assoc()) {
                    $users[] = $row;
                }
                header('Content-Type: application/json');
                echo json_encode($users);
            } else
                throw new Exception("По запросу ничего не найдено");
        } else
            throw new Exception("Введите ключевое слово для поиска");

    }
    else
        throw new Exception("Запрос для поиска не был передан");
}
catch (Exception $e)
{
    http_response_code(404);
    echo $e->getMessage();
}
finally {
    $conn->close();
}
