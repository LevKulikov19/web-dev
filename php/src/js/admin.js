document.addEventListener('DOMContentLoaded', function() {
    var formWord = document.querySelector('form.findUserWord');
    var messageFindUserWord = document.querySelector('.messageFindUserWord');
    var tableFindUserWord = document.querySelector('.tableFindUserWord');
    formWord.addEventListener('submit', function(e) {
        e.preventDefault();
        tableFindUserWord.querySelector('tbody').innerHTML = '';
        tableFindUserWord.style.display = "none";
        messageFindUserWord.style.display = "none";
        console.log(formWord);
        var formDataWord = new FormData(formWord);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", window.location.origin+"/findUserWord", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        console.log(new URLSearchParams(formDataWord).toString());
        xhr.send(new URLSearchParams(formDataWord).toString());

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                var status = xhr.status;
                var response = xhr.responseText;
                if (status >= 200 && status < 300) {
                    response = JSON.parse(xhr.responseText);
                    for (let i = 0; i < response.length; i++) {
                        // Создаем новую строку и ячейки для каждого элемента данных
                        let row = document.createElement('tr');
                        let firstNameCell = document.createElement('td');
                        let lastNameCell = document.createElement('td');
                        let emailCell = document.createElement('td');
                        let ageCell = document.createElement('td');
                        let genderCell = document.createElement('td');
                        let dateCreateCell = document.createElement('td');

                        // Заполняем ячейки данными
                        firstNameCell.textContent = response[i].firstName;
                        lastNameCell.textContent = response[i].lastName;
                        emailCell.textContent = response[i].email;
                        ageCell.textContent = response[i].age;
                        genderCell.textContent = response[i].gender === '1' ? 'Мужской' : 'Женский';
                        dateCreateCell.textContent = response[i].dateCreate;

                        // Добавляем ячейки в строку
                        row.appendChild(firstNameCell);
                        row.appendChild(lastNameCell);
                        row.appendChild(emailCell);
                        row.appendChild(ageCell);
                        row.appendChild(genderCell);
                        row.appendChild(dateCreateCell);

                        // Добавляем строку в таблицу
                        tableFindUserWord.querySelector('tbody').appendChild(row);
                    }
                    tableFindUserWord.style.display = "block";

                } else {
                    messageFindUserWord.innerText = response;
                    messageFindUserWord.style.display = "block";
                }
            }
        }
    });
});

document.addEventListener('DOMContentLoaded', function() {
    var formPhrase = document.querySelector('form.findUserPhrase');
    var messageFindUserPhrase = document.querySelector('.messageFindUserPhrase');
    var tableFindUserPhrase = document.querySelector('.tableFindUserPhrase');
    formPhrase.addEventListener('submit', function(e) {
        e.preventDefault();
        tableFindUserPhrase.querySelector('tbody').innerHTML = '';
        tableFindUserPhrase.style.display = "none";
        messageFindUserPhrase.style.display = "none";
        console.log(formPhrase);
        var formDataWord = new FormData(formPhrase);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", window.location.origin+"/findUserPhrase", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        console.log(new URLSearchParams(formDataWord).toString());
        xhr.send(new URLSearchParams(formDataWord).toString());

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                var status = xhr.status;
                var response = xhr.responseText;
                if (status >= 200 && status < 300) {
                    response = JSON.parse(xhr.responseText);
                    for (let i = 0; i < response.length; i++) {
                        // Создаем новую строку и ячейки для каждого элемента данных
                        let row = document.createElement('tr');
                        let firstNameCell = document.createElement('td');
                        let lastNameCell = document.createElement('td');
                        let emailCell = document.createElement('td');
                        let ageCell = document.createElement('td');
                        let genderCell = document.createElement('td');
                        let dateCreateCell = document.createElement('td');

                        // Заполняем ячейки данными
                        firstNameCell.textContent = response[i].firstName;
                        lastNameCell.textContent = response[i].lastName;
                        emailCell.textContent = response[i].email;
                        ageCell.textContent = response[i].age;
                        genderCell.textContent = response[i].gender === '1' ? 'Мужской' : 'Женский';
                        dateCreateCell.textContent = response[i].dateCreate;

                        // Добавляем ячейки в строку
                        row.appendChild(firstNameCell);
                        row.appendChild(lastNameCell);
                        row.appendChild(emailCell);
                        row.appendChild(ageCell);
                        row.appendChild(genderCell);
                        row.appendChild(dateCreateCell);

                        // Добавляем строку в таблицу
                        tableFindUserPhrase.querySelector('tbody').appendChild(row);
                    }
                    tableFindUserPhrase.style.display = "block";

                } else {
                    messageFindUserPhrase.innerText = response;
                    messageFindUserPhrase.style.display = "block";
                }
            }
        }
    });
});