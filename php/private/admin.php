<?php
function render_admin ($userActive, $usersSurname, $userCount, $userCountLastMonth, $userLast): void
{
echo '
<!DOCTYPE html>
<html>
<head>
    <title>Панель администратора</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/darkTheme.js" defer></script>
    <link rel="stylesheet" href="css/darkTheme.css">
    <script src="js/logout.js" defer></script>
    <script src="js/admin.js" defer></script>
</head>
<body class="light-mode">
<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#">Администратор</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Пользователи</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Настройки</a>
            </li>
        </ul>
    </div>
    <div class="">
        <button id="themeButton" class="btn btn-secondary">Темная тема</button>
        <a href="/" class="btn btn-primary" onclick="logout()">Выйти</a>
    </div>
</nav>
<div class="container mt-3">
    <h2>Здравствуйте, ' . $userActive['firstName'] . '</h2>
    <h2>Добро пожаловать в панель администратора</h2>
    
    <h3 class="mt-5">Фамилии всех зарегистрированных пользователей</h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Фамилии</th>
            </tr>
        </thead>
        <tbody>';

    for ($i = 1; $i <= count($usersSurname); $i++) {
        echo "<tr>";
        echo "<th scope='row'>" . $i . "</th>";
        echo "<td>" . $usersSurname[$i-1] . "</td>";
        echo "</tr>";
    }

    echo '
        </tbody>
    </table>
    
    <h3 class="mt-5">Общее количество зарегистрированных пользоватлей</h3>
    <span>Количество: ' . $userCount . '</span>
    
    <h3 class="mt-5">Количество пользователей за последний месяц</h3>
    <span>Количество: ' . $userCountLastMonth . '</span>
    
    <h3 class="mt-5">Послейдний зарегестрированнй пользователь</h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">E-mail</th>
                <th scope="col">Возраст</th>
                <th scope="col">Пол</th>
                <th scope="col">Дата регистации</th>
            </tr>
        </thead>
        <tbody>';
        $genderLastUser = $userLast['gender'] == 1 ? "Мужчина" : "Женщина";
        $ageLastUser = $userLast['age'] == 0 ? "Не совершеннолетний" : "Совершеннолетний";
        echo "<tr>";
        echo "<td>" . $userLast['firstName'] . "</td>";
        echo "<td>" . $userLast['lastName'] . "</td>";
        echo "<td>" . $userLast['email'] . "</td>";
        echo "<td>" . $ageLastUser . "</td>";
        echo "<td>" . $genderLastUser . "</td>";
        echo "<td>" . $userLast['dateCreate'] . "</td>";
        echo "</tr>";


    echo '
        </tbody>
    </table>
        
    <h3 class="mt-5">Поиск пользователей по слову</h3>
    <form class="form-inline my-2 my-lg-0 findUserWord">
      <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Поиск" name="search" required>
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
    </form>
    <p class="messageFindUserWord mt-3 alert alert-info" style="display: none"></p>
    <table class="tableFindUserWord table mt-3" style="display: none">
        <thead>
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">E-mail</th>
                <th scope="col">Возраст</th>
                <th scope="col">Пол</th>
                <th scope="col">Дата регистации</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    
    <h3 class="mt-5">Поиск пользователей по фразе</h3>
    <form class="form-inline my-2 my-lg-0 findUserPhrase">
      <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Поиск" name="search" id="search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
    </form>
    <p class="messageFindUserPhrase mt-3 alert alert-info" style="display: none"></p>
    <table class="tableFindUserPhrase table mt-3" style="display: none">
        <thead>
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">E-mail</th>
                <th scope="col">Возраст</th>
                <th scope="col">Пол</th>
                <th scope="col">Дата регистации</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</body>
</html>';
}

function render_admin_guest (): void
{
    echo '
<!DOCTYPE html>
<html>
<head>
    <title>Панель администратора</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/darkTheme.js" defer></script>
    <link rel="stylesheet" href="css/darkTheme.css">
</head>
<body class="light-mode">
<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#">Администратор</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Пользователи</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Настройки</a>
            </li>
        </ul>
    </div>
    <div class="">
        <button id="themeButton" class="btn btn-secondary">Темная тема</button>
        <a href="/" class="btn btn-primary">Войти</a>
    </div>
</nav>
<div class="container mt-3">
    <h2>Здравствуйте, вы вошли в систему как гость</h2>
</div>
</body>
</html>

';
}