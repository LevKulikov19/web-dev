<?php
function render_error_login($error_message): void
{
    echo '
<!DOCTYPE html>
<html>
<head>
    <title>Ошибка входа</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="darkTheme.js" defer></script>
    <link rel="stylesheet" href="darkTheme.css">
</head>
<body>
    <header class="d-flex flex-row-reverse w-75 p-3">
        <button id="themeButton" class="btn btn-secondary mt-3 ">Темная тема</button>
    </header>
    <div class="container">
        <div class="alert alert-danger mt-5" role="alert">
            <h4 class="alert-heading">Ошибка входа!</h4>
            <p>Вход в систему не был выполнен корректно. Пожалуйста, проверьте введенные данные и попробуйте снова.</p>
            <p>' . $error_message . '</p>
            <hr>
            <p class="mb-0">Если проблема повторяется, обратитесь в службу поддержки.</p>
        </div>
        <a href="/" class="btn btn-primary mt-3">Вернуться к форме входа</a>
        <a href="/signup" class="btn btn-primary mt-3">Вернуться к форме регистрации</a>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
';
}

function error_code_to_message($code): string
{
    return match ($code) {
        1 => "Имя обязательно для заполнения",
        2 => "Фамилии обязательна для заполнения",
        3 => "e-mail обязателен для заполнения",
        4 => "Логин обязателен для заполнения",
        5 => "Пароль обязателен для заполнения",
        6 => "Подтверждение пароля обязательно для заполнения",
        7 => "Возраст обязателен для заполнения",
        8 => "Поле пола не должно быть пустым",
        9 => "Поле принятия пользовательского соглашения не должно быть пустым",
        101 => "Только буквы разрешены в имени, длина от 2 до 15 символов",
        102 => "Только буквы разрешены в фамилии, длина от 2 до 15 символов, также разрешены 2 фамилии через дефиз",
        103 => "Неверный формат email",
        104 => "Только буквы и цифры разрешены в логине",
        105 => "Пароль должен содержать минимум одну цифру, одну заглавную и одну строчную букву, и быть не менее 8 символов",
        106 => "Пароль потверждения должен совпадать с исходным паролем",
        107 => "Вы должны указать корретный возраст",
        108 => "Не вервый параметр пола",
        109 => "Вы должны принять условия пользовательского соглашения",
        110 => "Логин уже занят другим пользователем",
        111 => "e-mail уже занят другим пользователем",
        112 => "Не верный логин или пароль",
        113 => "Не прошла проверку recaptcha",
        1000 => "Ошибка подключения",
        default => "Неизвестная ошибка",
    };
}