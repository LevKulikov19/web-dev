var isDark = 0;
document.getElementById('themeButton').addEventListener('click', function() {
    var body = document.body;

    if (body.classList.contains('light-mode')) {
        body.classList.remove('light-mode');
        body.classList.add('dark-mode');
        this.textContent = 'Светлая тема';
        isDark = 0;
    } else {
        body.classList.remove('dark-mode');
        body.classList.add('light-mode');
        this.textContent = 'Темная тема';
        isDark = 1;
    }

    var token = getCookie("token");
    if (typeof token === "undefined") return;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", window.location.origin+"/updateStateTheme", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("token=" + encodeURIComponent(token) + "&darkTheme=" + !isDark);
});


window.addEventListener('load', (event) => {
    var theme = getCookie("darkTheme");
    var themeButton = document.getElementById('themeButton');
    if (typeof theme === "undefined") return;
    if (theme === "1") {
        var body = document.body;
        body.classList.remove('light-mode');
        body.classList.add('dark-mode');
        themeButton.textContent = 'Светлая тема';
        isDark = 1;
    }
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}