from django.db import models

class User(models.Model):
    firstName = models.CharField(max_length=255, null=False)
    lastName = models.CharField(max_length=255, null=False)
    email = models.EmailField(unique=True, null=False)
    login = models.CharField(max_length=255, unique=True, null=False)
    password = models.CharField(max_length=255, null=False)
    age = models.BooleanField(null=False)
    gender = models.BooleanField(null=False)
    dateCreate = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.firstName
