from django import forms
from django.core.validators import RegexValidator
from .models import User

class SignupForm(forms.Form):
    firstName = forms.CharField(
        max_length=15,
        validators=[RegexValidator(r'^[a-zA-Zа-яА-Я]{2,15}$')],
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'firstName'})
    )
    lastName = forms.CharField(
        max_length=30,
        validators=[RegexValidator(r'^([a-zA-Zа-яА-Я-]{2,15}\s?){1,2}$')],
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'lastName'})
    )
    email = forms.EmailField(
        validators=[RegexValidator(r'^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')],
        required=True,
        widget=forms.EmailInput(attrs={'class': 'form-control', 'id': 'email'})
    )
    login = forms.CharField(
        min_length=6,
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'login'})
    )
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password'}),
        validators=[RegexValidator(r'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')],
        required=True
    )
    confirmPassword = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'confirmPassword'}),
        required=True
    )
    age = forms.ChoiceField(
        choices=[('18', 'Мне 18 лет'), ('under_18', 'Нет 18 лет')],
        required=True,
        widget=forms.Select(attrs={'class': 'form-control', 'id': 'age'})
    )
    gender = forms.ChoiceField(
        choices=[('male', 'Мужской'), ('female', 'Женский')],
        widget=forms.RadioSelect(attrs={'class': 'form-check-input'}),
        initial='male',
        required=True
    )
    terms = forms.BooleanField(
        required=True,
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'id': 'terms'})
    )

    class Meta:
        model = User


class SigninForm(forms.Form):
    login = forms.CharField(
        min_length=6,
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
            'pattern': '.{6,}',
            'required': True
        })
    )
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'id': 'password',
            'pattern': '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
            'required': True
        }),
        validators=[RegexValidator(r'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')],
        required=True
    )