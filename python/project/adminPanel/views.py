import uuid
from django.shortcuts import render, redirect
from django.contrib.auth.hashers import make_password, check_password
from .forms import SigninForm
from .forms import SignupForm
from .models import User
import requests
from django.utils import timezone
from datetime import timedelta
from django.http import JsonResponse
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt

def signinUser(request):
    try:
        if request.method == 'POST':
            form = SigninForm(request.POST)
            recaptcha_response = request.POST.get('g-recaptcha-response')
            if not check_recaptcha(recaptcha_response):
                form.add_error(None, "Ошибка reCAPTCHA.")
            elif form.is_valid():
                username = form.cleaned_data.get('login')
                password = form.cleaned_data.get('password')
                # Получаем пользователя из модели User
                user = User.objects.filter(login=username).first()
                if user is not None and check_password(password, user.password):
                    token = uuid.uuid4()
                    user.token = str(token)
                    user.save()
                    response = redirect('admin')
                    response.set_cookie('token', str(token))
                    return response
                else:
                    form.add_error(None, "Неправильное имя пользователя или пароль.")
        else:
            form = SigninForm()
    except Exception as e:
        form.add_error(None, f"Произошла ошибка при работе: {str(e)}")
    return render(request, 'admin/signinUser.html', {'form': form})

@csrf_exempt
def signup(request):
    try:
        if request.method == 'POST':
            print(request.POST)
            form = SignupForm(request.POST)
            recaptcha_response = request.POST.get('g-recaptcha-response')
            if not check_recaptcha(recaptcha_response):
                form.add_error(None, "Ошибка reCAPTCHA.")
            else:
                if form.is_valid():
                    email = form.cleaned_data.get('email')
                    login = form.cleaned_data.get('login')
                    try:
                        user = User.objects.get(email=email)
                        raise Exception('Email уже существует')
                    except User.DoesNotExist:
                        pass

                    try:
                        user = User.objects.get(login=login)
                        raise Exception('Логин уже существует')
                    except User.DoesNotExist:
                        pass

                    if form.is_valid():
                        user = User(
                            firstName=form.cleaned_data.get('firstName'),
                            lastName=form.cleaned_data.get('lastName'),
                            email=email,
                            login=login,
                            password=make_password(form.cleaned_data.get('password')),
                            age=form.cleaned_data.get('age') == '18',
                            gender=form.cleaned_data.get('gender') == 'male',
                        )
                        user.save()

                        token = uuid.uuid4()
                        user.token = str(token)
                        user.save()
                        form=SignupForm()
                        response = redirect('admin')
                        response.set_cookie('token', str(token))
                        return response
                else:
                    form = SignupForm()
        else:
            form = SignupForm()
    except Exception as e:
        form.add_error(None, f"Произошла ошибка: {str(e)}")
    return render(request, 'admin/signup.html', {'form': form})



def admin(request):
    token = request.COOKIES.get('token')
    if token is None:
        return render(request, 'admin/adminGuset.html')
    else:
        user = User.objects.filter(token=token).first()
        if user is not None:
            usersSurname = get_all_surnames()
            userCount = get_user_count()
            userCountLastMonth = get_user_count_last_month()
            userLast = get_user_last()
            context = {
                'userActive': user,
                'usersSurname': usersSurname,
                'userCount': userCount,
                'userCountLastMonth': userCountLastMonth,
                'userLast': userLast,
            }
            return render(request, 'admin/admin.html', context)
        else:
            return render(request, 'admin/adminGuset.html')

def check_recaptcha(response):
    data = {
        'secret': '6LfoEJQpAAAAAC9rqJlmGETzGfrS0P64X3bl50Kw',
        'response': response
    }
    r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
    result = r.json()
    print(result)
    if result['success']:
        return True
    else:
        return False


def get_all_surnames():
    return User.objects.values_list('lastName', flat=True)

def get_user_count():
    return User.objects.count()

def get_user_count_last_month():
    begin_date = timezone.now().replace(day=1)
    end_date = (begin_date + timedelta(days=32)).replace(day=1)
    return User.objects.filter(dateCreate__range=[begin_date, end_date]).count()

def get_user_last():
    return User.objects.latest('dateCreate')

def findUserPhrase(request):
    token = request.COOKIES.get('token')
    if token is None:
        return JsonResponse({"error": "Forbidden"}, status=403)
    user = User.objects.filter(token=token).first()
    if user is None:
        return JsonResponse({"error": "Forbidden"}, status=403)
    if request.method == 'POST':
        search_query = request.POST.get('search', None)
        if search_query:
            search_words = search_query.split(' ')
            query = Q()
            for word in search_words:
                if word:
                    query |= Q(firstName__icontains=word)
            users = User.objects.filter(query)
            if users.exists():
                response = list(users.values())
                return JsonResponse(response, safe=False)
            else:
                return JsonResponse({"error": "По запросу ничего не найдено"}, status=404)
        else:
            return JsonResponse({"error": "Введите запрос для поиска"}, status=400)
    else:
        return JsonResponse({"error": "Invalid Method"}, status=400)

def findUserWord(request):
    token = request.COOKIES.get('token')
    if token is None:
        return JsonResponse({"error": "Forbidden"}, status=403)
    user = User.objects.filter(token=token).first()
    if user is None:
        return JsonResponse({"error": "Forbidden"}, status=403)
    if request.method == 'POST':
        user_search = request.POST.get('search', None)
        if user_search:
            users = User.objects.filter(firstName__icontains=user_search)
            if users.exists():
                response = list(users.values())
                return JsonResponse(response, safe=False)
            else:
                return JsonResponse({"error": "По запросу ничего не найдено"}, status=404)
        else:
            return JsonResponse({"error": "Введите ключевое слово для поиска"}, status=400)
    else:
        return JsonResponse({"error": "Запрос для поиска не был передан"}, status=400)