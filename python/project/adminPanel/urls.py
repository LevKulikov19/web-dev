from django.urls import path
from . import views

urlpatterns = [
    path('', views.signinUser, name='signinUser'),
    path('signup', views.signup, name='signup'),
    path('admin', views.admin, name='admin'),
    path('findUserPhrase', views.findUserPhrase, name='findUserPhrase'),
    path('findUserWord', views.findUserWord, name='findUserWord'),
]
