const axios = require('axios');


async function signup(firstName, lastName, email, login, password,
                      confirmPassword, ageBool, genderBool, termsBool,
                      recaptchaToken) {
    const url = `${process.env.VUE_APP_BACKEND_BASE_URL}/auth/singup`;
    const gender = genderBool ? 'male' : 'female';
    const age = ageBool ? 'Мне 18 лет' : 'Нет 18 лет';
    const terms = termsBool ? 'on' : 'off';
    const data = {
        firstName,
        lastName,
        email,
        login,
        password,
        confirmPassword,
        age,
        gender,
        terms,
        recaptchaToken
    };
    console.log(data)
    try {
        let response = await axios.post(url, data);
        response = response.data;
        localStorage.setItem('accessToken', response.accessToken);
        localStorage.setItem('refreshToken', response.refreshToken);
    } catch (error) {
        throw new Error(error.response.data.message || error.message);
    }

}

async function login(login, password, recaptchaToken) {
    const url = `${process.env.VUE_APP_BACKEND_BASE_URL}/auth/login`;
    console.log(url)
    const data = {
        login,
        password,
        recaptchaToken
    };

    try {
        let response = await axios.post(url, data);
        console.log("asd");
        response = response.data;
        localStorage.setItem('accessToken', response.accessToken);
        localStorage.setItem('refreshToken', response.refreshToken);

        return response.data;
    } catch (error) {
        throw new Error(error.response.data.message || error.message);
    }
}


async function refresh(refreshToken) {
    console.log(refreshToken)
    const url = `${process.env.VUE_APP_BACKEND_BASE_URL}/auth/refresh`;
    const data = {
        refreshToken
    };

    try {
        let response = await axios.post(url, data);
        response = response.data;
        localStorage.setItem('accessToken', response.accessToken);
    } catch (error) {
        console.error(error);
    }
}

async function logout() {
    console.log("asdad");
    const url = `${process.env.VUE_APP_BACKEND_BASE_URL}/auth/logout`;
    const refreshToken = localStorage.getItem('refreshToken');
    const data = {
        refreshToken
    };

    try {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        const response = await axios.post(url, data);
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

async function getToken() {
    let tokenData = localStorage.getItem('accessToken');
    if (Date.now() >= getJwtExpirationDate(tokenData)) { // проверяем не истек ли срок жизни токена
        console.log("обновление токена");
        try {
            await refresh(localStorage.getItem('refreshToken'));
        } catch (e) {
           return  window.location.replace("/");
        }
    }

    return localStorage.getItem('accessToken');
}

function getJwtExpirationDate(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    const { exp } = JSON.parse(jsonPayload);
    if (!exp) {
        return null;
    }

    return exp * 1000;
}

function isAuth() {
    return localStorage.getItem('accessToken') !== null;
}

function getNameCurrentUser() {
    if (isAuth())
        return getPayloadFromJwt(localStorage.getItem('accessToken')).name;
    else
        return null;
}

function getPayloadFromJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}


module.exports = {
    signup,
    login,
    refresh,
    logout,
    getToken,
    isAuth,
    getNameCurrentUser
}