document.addEventListener('DOMContentLoaded', function() {
    var isDark = 0;
    document.getElementById('themeButton').addEventListener('click', function() {
        var body = document.body;

        if (!body.classList.contains('dark-mode')) {
            body.classList.remove('light-mode');
            body.classList.add('dark-mode');
            this.textContent = 'Светлая тема';
            isDark = 0;
        } else {
            body.classList.remove('dark-mode');
            body.classList.add('light-mode');
            this.textContent = 'Темная тема';
            isDark = 1;
        }

        localStorage.setItem("darkTheme", !isDark);
    });

    window.addEventListener('load', (event) => {
        var theme = localStorage.getItem("darkTheme");
        var themeButton = document.getElementById('themeButton');
        if (theme === null) return;
        if (theme === "true") {
            var body = document.body;
            body.classList.remove('light-mode');
            body.classList.add('dark-mode');
            themeButton.textContent = 'Светлая тема';
            isDark = 1;
        }
    });
});
