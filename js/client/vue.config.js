const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  pages: {
    index: {
      // точка входа для страницы
      entry: 'src/main.js',
      // исходный шаблон
      template: 'public/index.html',
      // в результате будет dist/index.html
      filename: 'index.html',
    }
  },
  devServer: {
    client: {
       overlay: {
          warnings: false,
          errors: true
        }
    }
  }
})
