const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  login: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  age: { type: Boolean, required: true },
  gender: { type: Boolean, required: true },
  darkTheme: { type: Boolean, default: false },
  dateCreate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('User', UserSchema);
