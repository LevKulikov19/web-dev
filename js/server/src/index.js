const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');
const { routes } = require("./routes");
const cors = require('cors');

mongoose.connect(
    'mongodb://localhost:27017/webdev'
);

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

routes.forEach((item) => {
    app.use(`/v1/${item}`, require(`./routes/${item}`));
});


const PORT = 3000;
http.createServer({}, app).listen(PORT);
console.log(`Server start on port: ${PORT}`);


