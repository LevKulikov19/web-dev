const router = require("express-promise-router")();

const { user } = require("../controllers");
const {checkJWTSign} = require('../middleware/jwtCheck.middleware');

router.route('/id/:id').get(checkJWTSign, user.get);
router.route('/').get(checkJWTSign,user.getAll);
router.route('/').put(checkJWTSign,user.update);
router.route('/allSurname/').get(checkJWTSign,user.getAllSurnames);
router.route('/count').get(checkJWTSign,user.getCount);
router.route('/countLastMonth').get(checkJWTSign,user.getCountLastMonth);
router.route('/last').get(checkJWTSign,user.getLast);
router.route('/findWord').get(checkJWTSign,user.findWord);
router.route('/findPhrase').get(checkJWTSign,user.findPhrase);

module.exports = router;