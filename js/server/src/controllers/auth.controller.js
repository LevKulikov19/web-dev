const { User, Token } = require("../model");
const { error_code_to_message } = require("../utils/errorCode");
const bcrypt = require('bcryptjs');
const axios = require('axios');
const jwt = require('jsonwebtoken');

async function singup(req, res) {
    try {
        const { firstName, lastName, email, login, password, confirmPassword, age, gender, terms, recaptchaToken } = req.body;
        const recaptchaSecret = '6LfoEJQpAAAAAC9rqJlmGETzGfrS0P64X3bl50Kw';
        const recaptchaVerifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${recaptchaSecret}&response=${recaptchaToken}`;

        const response = await axios.post(recaptchaVerifyUrl);
        if (response.data.success !== true) throw new Error(error_code_to_message(113));

        if (!firstName)
            throw new Error(error_code_to_message(1));
        if (!lastName)
            throw new Error(error_code_to_message(2));
        if (!email)
            throw new Error(error_code_to_message(3));
        if (!login)
            throw new Error(error_code_to_message(4));
        if (!password)
            throw new Error(error_code_to_message(5));
        if (!confirmPassword)
            throw new Error(error_code_to_message(6));
        if (!age)
            throw new Error(error_code_to_message(7));
        if (!gender)
            throw new Error(error_code_to_message(8));
        if (!terms)
            throw new Error(error_code_to_message(9));

        if (!validate_name(firstName))
            throw new Error(error_code_to_message(101));
        if (!validate_surname(lastName))
            throw new Error(error_code_to_message(102));
        if (!validate_email(email))
            throw new Error(error_code_to_message(103));
        if (!validate_login(login))
            throw new Error(error_code_to_message(104));
        if (!validate_password(password))
            throw new Error(error_code_to_message(105));
        if (!validate_confirm_password(confirmPassword, password))
            throw new Error(error_code_to_message(106));
        if (!validate_age(age))
            throw new Error(error_code_to_message(107));
        if (!validate_gender(gender))
            throw new Error(error_code_to_message(108));
        if (!validate_terms(terms))
            throw new Error(error_code_to_message(109));


        let user = await User.findOne({ login });
        if (user) throw new Error(error_code_to_message(110));

        user = await User.findOne({ email });
        if (user) throw new Error(error_code_to_message(111));

        const password_hashed = await bcrypt_hash(password);
        console.log(password_hashed)

        const age_db = age === "Мне 18 лет";
        const gender_db = gender === "male";

        console.log("asd")
        user = new User({
            firstName,
            lastName,
            email,
            login,
            password: password_hashed,
            age: age_db,
            gender: gender_db,
        });

        await user.save();

        const foundUser = await User.findOne({ login })

        const accessToken = jwt.sign({
          userId: foundUser._id,
          name: foundUser.name,
          login: foundUser.login,
        }, process.env.JWT_SECRET, {
          expiresIn: '1m'
        })

        const refreshToken = jwt.sign({
          userId: foundUser._id,
          name: foundUser.name,
          login: foundUser.login,
        }, process.env.JWT_SECRET_REFRESH)

        const foundToken = await Token.findOne({
          user: foundUser._id
        })

        if (foundToken) {
          await Token.findByIdAndUpdate(foundToken._id, { token: refreshToken })
          return res.status(200).send({
            accessToken,
            refreshToken,
            name: foundUser.name,
            login: foundUser.login,
          })
        }

        const item = new Token({ token: refreshToken, user: foundUser._id });
        await item.save();

        return res.status(200).send({
          accessToken,
          refreshToken,
          name: foundUser.name,
          login: foundUser.login,
        })
    } catch (err) {
        return res.status(403).send({
            message: err.message,
            err
        })
    }
}

async function bcrypt_hash(password) {
    const saltValue = await bcrypt.genSalt(10);
    return bcrypt.hashSync("B4c0/\/", saltValue);
  }

async function login(req, res) {
    try {
        const { login, password, recaptchaToken } = req.body;
        const recaptchaSecret = '6LfoEJQpAAAAAC9rqJlmGETzGfrS0P64X3bl50Kw';
        const recaptchaVerifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${recaptchaSecret}&response=${recaptchaToken}`;

        const response = await axios.post(recaptchaVerifyUrl);
        if (response.data.success !== true) throw new Error(error_code_to_message(113));

        const foundUser = await User.findOne({ login })

        if (!foundUser) {
          return res.status(403).send({
            message: error_code_to_message(112),
            err
          })
        }

        const isPasswordCorrect = bcrypt.compare(password, foundUser.password)

        if (!isPasswordCorrect) {
          return res.status(403).send({
            message: error_code_to_message(112),
            err
          })
        }

        const accessToken = jwt.sign({
          userId: foundUser._id,
          name: foundUser.name,
          login: foundUser.login,
        }, process.env.JWT_SECRET, {
          expiresIn: '1m'
        })

        const refreshToken = jwt.sign({
          userId: foundUser._id,
          name: foundUser.name,
          login: foundUser.login,
        }, process.env.JWT_SECRET_REFRESH)

        const foundToken = await Token.findOne({
          user: foundUser._id
        })

        if (foundToken) {
          await Token.findByIdAndUpdate(foundToken._id, { token: refreshToken })
          return res.status(200).send({
            accessToken,
            refreshToken,
            name: foundUser.name,
            login: foundUser.login,
          })
        }

        const item = new Token({ token: refreshToken, user: foundUser._id });
        await item.save();

        return res.status(200).send({
          accessToken,
          refreshToken,
          name: foundUser.name,
          login: foundUser.login,
        })

      } catch (err) {
        return res.status(403).send({
            message: err.message,
            err
        })
      }
}



async function logout(req, res) {
    const { refreshToken } = req.body;
    const foundToken = await Token.findOne({ token: refreshToken })

    if (!foundToken) {
      return res.status(403).send({
        message: 'Пользователь не авторизован'
      })
    }

    await Token.findByIdAndDelete(foundToken._id)

    return res.status(200).send({
      message: 'Пользователь успешно вышел из системы'
    })
}

async function refreshToken(req, res) {
    const { refreshToken } = req.body;
    if (!refreshToken) {
        return res.status(403).send({
          message: error_code_to_message(1001)
        })
      }
      const currentToken = await Token.findOne({ token: refreshToken })
      if (!currentToken) {
        return res.status(403).send({
          message: error_code_to_message(1001)
        })
      }

      jwt.verify(refreshToken, process.env.JWT_SECRET_REFRESH, (err, user) => {
        if (err) {
          return res.status(403).send({
            message: error_code_to_message(1001)
          })
        }

        const accessToken = jwt.sign({
          userId: user._id,
          login: user.login,
        }, process.env.JWT_SECRET, {
          expiresIn: '1m'
        })

        return res.status(200).send({
          accessToken,
          login: user.login
        })

      })

}

module.exports = {
    singup,
    login,
    logout,
    refreshToken
};

function test_input(data) {
    data = data.trim();
    data = data.replace(/\\/g, '');
    return data.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
}

function validate_name(name) {
    name = test_input(name);
    let pattern = /^[a-zA-Zа-яА-Я]{2,15}$/u;
    return pattern.test(name);
}

function validate_surname(name) {
    name = test_input(name);
    let pattern = /^([a-zA-Zа-яА-Я-]{2,15}\s?){1,2}$/u;
    return pattern.test(name);
}

function validate_email(email) {
    email = test_input(email);
    let pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return pattern.test(email);
}

function validate_login(login) {
    login = test_input(login);
    let pattern = /^[a-zA-Z0-9]*$/;
    return pattern.test(login);
}

function validate_password(password) {
    password = test_input(password);
    let pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    return pattern.test(password);
}

function validate_confirm_password(confirmPassword, password) {
    confirmPassword = test_input(confirmPassword);
    return confirmPassword === password;
}

function validate_age(age) {
    return (age === "Мне 18 лет") || (age === "Нет 18 лет");
}

function validate_gender(gender) {
    return (gender === "male") || (gender === "female");
}

function validate_terms(terms) {
    return terms === "on";
}
