const genericCrud = require("./generic.controller");
const { User } = require("../model");
const boom = require("boom");

async function getAllSurnames(_, res) {
    try {
        const users = await User.find({}, 'lastName');
        const surnames = users.map(user => user.lastName);
        const uniqueSurnames = [...new Set(surnames)];
        return res.status(200).send(uniqueSurnames);
    } catch (err) {
        return res.status(400).send(boom.boomify(err));
    }
}

async function getCount(_, res) {
    try {
        const count = await User.countDocuments();
        return res.status(200).send({"userCount": count});
    } catch (err) {
        return res.status(400).send(boom.boomify(err));
    }
}

async function getCountLastMonth(_, res) {
    try {
        const oneMonthAgo = new Date();
        oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
        const count = await User.countDocuments({ dateCreate: { $gte: oneMonthAgo } });
        return res.status(200).send({"userCountLastMonth": count});
    } catch (err) {
        return res.status(400).send(boom.boomify(err));
    }
}

async function getLast(_, res) {
    try {
        const user = await User.findOne().sort({ dateCreate: -1 });
        return res.status(200).send({"userLast": user});
    } catch (err) {
        return res.status(400).send(boom.boomify(err));
    }
}

async function findPhrase(req, res) {
    try {
        if (!req.query.search) {
            throw new Error("Введите запрос для поиска");
        }

        const searchWords = req.query.search.split(' ');
        const searchQueries = searchWords.map(word => ({ firstName: new RegExp(word, 'i') }));
        const users = await User.find({ $or: searchQueries });
        if (users.length > 0) {

            return res.status(200).send(users);
        } else {
          res.status(404).send('По запросу ничего не найдено');
        }
      } catch (err) {
        res.status(500).send(err.message);
      }
}

async function findWord(req, res) {
    try {
        if (!req.query.search) {
            throw new Error("Введите запрос для поиска");
        }

        const searchWords = req.query.search.split(' ');
        const searchQueries = searchWords.map(word => ({ firstName: new RegExp(word, 'i') }));

        const users = await User.find({ $or: searchQueries });

        if (users.length === 0) {
            throw new Error("По запросу ничего не найдено");
        }

        return res.status(200).send(users);
    } catch (error) {
        res.status(404).send(error.message);
    }
}

module.exports = {
    ...genericCrud(User),
    getAllSurnames,
    getCount,
    getCountLastMonth,
    getLast,
    findWord,
    findPhrase
};